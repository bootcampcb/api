﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AnimalShelter;
using AnimalShelter.Models;

namespace AnimalShelter.Controllers
{
    [Route("api/[controller]")]
    [System.Web.Mvc.RoutePrefix("api/books")]
    [ApiController]
    public class AnimalsController : ControllerBase
    {
        private readonly AnimalManager _animalManager;

        public AnimalsController(AnimalManager animalManager)
        {
            _animalManager = animalManager;
        }

        // GET: api/Animals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Animal>>> GetAnimals()
        {
            var animals = await _animalManager.GetAllAnimalsAsync();
            return Ok(animals);
        }

        // GET: api/Animals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Animal>> GetAnimal(int id)
        {
            var animal = await _animalManager.GetAnimalByIdAsync(id);
            if (animal == null)
            {
                return NotFound();
            }

            return Ok(animal);
        }

        [Route("{owner}/owner")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Animal>>> GetAnimalsByOwner(string owner)
        {
            var animals = await _animalManager.GetAnimalsByOwnerAsync(owner);
            if (animals == null || !animals.Any())
            {
                return NotFound();
            }

            return Ok(animals);
        }

        // PUT: api/Animals/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAnimal(int id, Animal animal)
        {
            if (id != animal.AnimalId)
            {
                return BadRequest();
            }

            var updatedAnimal = await _animalManager.UpdateAnimalAsync(id, animal);
            if (updatedAnimal == null)
            {
                return NotFound();
            }
            return NoContent();
        }
        // POST: api/Animals
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Animal>> PostAnimal(Animal animal)
        {
            await _animalManager.AddAnimalAsync(animal);

            return CreatedAtAction("GetAnimal", new { id = animal.AnimalId }, animal);
        }

        // DELETE: api/Animals/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAnimal(int id)
        {
            var deletedAnimal = await _animalManager.DeleteAnimalAsync(id);
            if (deletedAnimal == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}

