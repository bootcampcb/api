﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AnimalShelter;
using AnimalShelter.Models;

namespace AnimalShelter.Models
{
    public class AnimalManager
    {
        private readonly AnimalShelterContext _context;

        public AnimalManager(AnimalShelterContext context)
        {
            _context = context;
        }

        public async Task<List<Animal>> GetAllAnimalsAsync()
        {
            return await _context.Animals.ToListAsync();
        }

        public async Task<Animal> GetAnimalByIdAsync(int id)
        {
            return await _context.Animals.FindAsync(id);
        }

        public async Task<List<Animal>> GetAnimalsByOwnerAsync(string owner)
        {
            return await _context.Animals.Where(a => a.Owner == owner).ToListAsync();
        }

        public async Task<bool> AddAnimalAsync(Animal animal)
        {
            if (_context.Animals == null)
            {
                return false;
            }

            _context.Animals.Add(animal);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateAnimalAsync(int id, Animal animal)
        {
            if (id != animal.AnimalId)
            {
                return false;
            }

            _context.Entry(animal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnimalExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        public async Task<bool> DeleteAnimalAsync(int id)
        {
            if (_context.Animals == null)
            {
                return false;
            }

            var animal = await _context.Animals.FindAsync(id);
            if (animal == null)
            {
                return false;
            }

            _context.Animals.Remove(animal);
            await _context.SaveChangesAsync();

            return true;
        }

        private bool AnimalExists(int id)
        {
            return (_context.Animals?.Any(e => e.AnimalId == id)).GetValueOrDefault();
        }
    }
}
