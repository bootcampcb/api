﻿using Microsoft.EntityFrameworkCore;
using AnimalShelter.Models;

namespace AnimalShelter
{
    public class AnimalShelterContext : DbContext
    {
        public AnimalShelterContext()
        {
        }

        public AnimalShelterContext(DbContextOptions<AnimalShelterContext> options)
            : base(options)
        {
        }
        public DbSet<Animal> Animals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=AnimalShelter;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>()
                .HasKey(a => a.AnimalId);

            modelBuilder.Entity<Animal>().HasData(
                new Animal { AnimalId = 1, Name = "Buddy", Species = "Dog", Breed = "Labrador Retriever", Owner = "John Smith", CheckInDate = DateTime.Parse("2022-01-01"), CheckOutDate = DateTime.Parse("2022-01-07") },
                new Animal { AnimalId = 2, Name = "Mittens", Species = "Cat", Breed = "Persian", Owner = "Jane Doe", CheckInDate = DateTime.Parse("2022-01-02"), CheckOutDate = DateTime.Parse("2022-01-09") },
                new Animal { AnimalId = 3, Name = "Rocky", Species = "Dog", Breed = "Bulldog", Owner = "Mike Johnson", CheckInDate = DateTime.Parse("2022-01-03"), CheckOutDate = DateTime.Parse("2022-01-08") },
                new Animal { AnimalId = 4, Name = "Fluffy", Species = "Cat", Breed = "Siamese", Owner = "Susan Lee", CheckInDate = DateTime.Parse("2022-01-04"), CheckOutDate = DateTime.Parse("2022-01-10") },
                new Animal { AnimalId = 5, Name = "Smokey", Species = "Dog", Breed = "Chihuahua", Owner = "Chris Davis", CheckInDate = DateTime.Parse("2022-01-05"), CheckOutDate = DateTime.Parse("2022-01-11") },
                new Animal { AnimalId = 6, Name = "Ginger", Species = "Cat", Breed = "Tabby", Owner = "Karen Wilson", CheckInDate = DateTime.Parse("2022-01-06"), CheckOutDate = DateTime.Parse("2022-01-12") },
                new Animal { AnimalId = 7, Name = "Rufus", Species = "Dog", Breed = "Golden Retriever", Owner = "Tom Williams", CheckInDate = DateTime.Parse("2022-01-07"), CheckOutDate = DateTime.Parse("2022-01-13") },
                new Animal { AnimalId = 8, Name = "Whiskers", Species = "Cat", Breed = "Calico", Owner = "Lisa Brown", CheckInDate = DateTime.Parse("2022-01-08"), CheckOutDate = DateTime.Parse("2022-01-14") },
                new Animal { AnimalId = 9, Name = "Max", Species = "Dog", Breed = "German Shepherd", Owner = "Andrew Martinez", CheckInDate = DateTime.Parse("2022-01-09"), CheckOutDate = DateTime.Parse("2022-01-15") },
                new Animal { AnimalId = 10, Name = "Lucky", Species = "Cat", Breed = "Russian Blue", Owner = "Emily Rodriguez", CheckInDate = DateTime.Parse("2022-01-10"), CheckOutDate = DateTime.Parse("2022-01-16") },
                new Animal { AnimalId = 11, Name = "Duke", Species = "Dog", Breed = "Boxer", Owner = "David Kim", CheckInDate = DateTime.Parse("2022-01-11"), CheckOutDate = DateTime.Parse("2022-01-17") },
                new Animal { AnimalId = 12, Name = "Tiger", Species = "Cat", Breed = "Bengal", Owner = "Jennifer Nguyen", CheckInDate = DateTime.Parse("2022-01-12"), CheckOutDate = DateTime.Parse("2022-01-18") });
        }
    }
}
