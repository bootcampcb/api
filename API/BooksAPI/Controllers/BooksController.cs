﻿using BooksAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace BooksAPI.Controllers
{
    [Route("api/[controller]")]
    [System.Web.Mvc.RoutePrefix("api/books")]
    [ApiController]
    public class BooksController : Controller
    {
        //private readonly Book[] _books = new Book[]
        //{
        //    new Book { BookId = 1, Title = "The Great Gatsby", Author = "F. Scott Fitzgerald", Publisher = "Charles Scribner's Sons", YearPublished = 1925, Price = 9.99 },
        //    new Book { BookId = 2, Title = "To Kill a Mockingbird", Author = "Harper Lee", Publisher = "J. B. Lippincott & Co.", YearPublished = 1960, Price = 12.50 },
        //    new Book { BookId = 3, Title = "The Catcher in the Rye", Author = "J.D. Salinger", Publisher = "Little, Brown and Company", YearPublished = 1951, Price = 8.95 },
        //    new Book { BookId = 4, Title = "1984", Author = "George Orwell", Publisher = "Secker & Warburg", YearPublished = 1949, Price = 10.99 },
        //    new Book { BookId = 5, Title = "The Hobbit", Author = "J.R.R. Tolkien", Publisher = "Allen & Unwin", YearPublished = 1937, Price = 14.99 },
        //    new Book { BookId = 6, Title = "Pride and Prejudice", Author = "Jane Austen", Publisher = "T. Egerton, Whitehall", YearPublished = 1813, Price = 7.99 },
        //    new Book { BookId = 7, Title = "The Lord of the Rings", Author = "J.R.R. Tolkien", Publisher = "Allen & Unwin", YearPublished = 1954, Price = 29.99 },
        //    new Book { BookId = 8, Title = "One Hundred Years of Solitude", Author = "Gabriel Garcia Marquez", Publisher = "Harper & Row", YearPublished = 1967, Price = 11.50 },
        //    new Book { BookId = 9, Title = "The Diary of a Young Girl", Author = "Anne Frank", Publisher = "Contact Publishing", YearPublished = 1947, Price = 6.99 },
        //    new Book { BookId = 10, Title = "The Alchemist", Author = "Paulo Coelho", Publisher = "HarperCollins", YearPublished = 1988, Price = 9.99 },
        //    new Book { BookId = 11, Title = "The Adventures of Huckleberry Finn", Author = "Mark Twain", Publisher = "Chatto & Windus", YearPublished = 1884, Price = 8.50 }
        //};

      //  GET api/books(return all books)
        [HttpGet]
        [Route("/all")]
        public IEnumerable<Book> Get()
        {
            var BookContext = new BookContext();
            // return array built above
            //return _books;
            return BookContext.Books;
        }

        [HttpGet]
        [Route("/count")]
        public int GetCount()
        {
            var BookContext = new BookContext();
            int bookCnt = BookContext.Books.Count();
            // return array built above
            //return _books;
            return BookContext.Books.Count();
        }
        // GET api/books/5 (return BookId where id=5)
        [Route("{bookId:int}/details")]
        [HttpGet]
        public ActionResult<Book> Get(int bookId)
        {
            // Retrieve from array
            //Book book = _books.FirstOrDefault(b => b.BookId == bookId);
            var BookContext = new BookContext();
            Book book = BookContext.Books.FirstOrDefault(b => b.BookId == bookId);

            if (book == null)
            {
                return NotFound();
            }
            return book;
        }
        [HttpPost]
        public ActionResult Post(Book book)
        {
            var BookContext = new BookContext();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

 //           book.BookId = BookContext.Books.Count() + 1;
            BookContext.Books.Add(book);
            BookContext.SaveChanges();

            string location = $"api/books/{book.BookId}";
            return Created(location, book);
        }
        [HttpPut("{bookId}")]
        public ActionResult Put(int bookId, Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var BookContext = new BookContext();
            var existingBook = BookContext.Books.FirstOrDefault(b => b.BookId == bookId);
            // This implementation updated the resource if found
            // or adds it if not found

            if (existingBook == null)
            {
                BookContext.Books.Add(book);
                BookContext.SaveChanges();
                string location =
                $"api/books/{book.BookId}";
                return Created(location, book);
            }
            else
            {
                existingBook.Title = book.Title;
                existingBook.Author = book.Author;
                existingBook.Publisher = book.Publisher;
                existingBook.YearPublished = book.YearPublished;
                existingBook.Price = book.Price;
                BookContext.SaveChanges();
                return Ok();
            }
        }
        [HttpDelete("{bookId}")]
        public ActionResult Delete(int bookId)
        {
            var BookContext = new BookContext();
            var existingBook = BookContext.Books.FirstOrDefault(b => b.BookId == bookId);
            if (existingBook == null)
            {
                return NotFound();
            }
            BookContext.Books.Remove(existingBook);
            BookContext.SaveChanges();
            return Ok();
        }
    }
}
