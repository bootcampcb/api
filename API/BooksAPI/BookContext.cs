﻿namespace BooksAPI
{
    using BooksAPI.Models;
    using Microsoft.EntityFrameworkCore;

    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=BooksDB;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                        .HasKey(b => b.BookId);
            modelBuilder.Entity<Book>().HasData(
                new Book { BookId = 1, Title = "The Great Gatsby", Author = "F. Scott Fitzgerald", Publisher = "Charles Scribner's Sons", YearPublished = 1925, Price = 9.99 },
                new Book { BookId = 2, Title = "To Kill a Mockingbird", Author = "Harper Lee", Publisher = "J. B. Lippincott & Co.", YearPublished = 1960, Price = 12.50 },
                new Book { BookId = 3, Title = "The Catcher in the Rye", Author = "J.D. Salinger", Publisher = "Little, Brown and Company", YearPublished = 1951, Price = 8.95 },
                new Book { BookId = 4, Title = "1984", Author = "George Orwell", Publisher = "Secker & Warburg", YearPublished = 1949, Price = 10.99 },
                new Book { BookId = 5, Title = "The Hobbit", Author = "J.R.R. Tolkien", Publisher = "Allen & Unwin", YearPublished = 1937, Price = 14.99 },
                new Book { BookId = 6, Title = "Pride and Prejudice", Author = "Jane Austen", Publisher = "T. Egerton, Whitehall", YearPublished = 1813, Price = 7.99 },
                new Book { BookId = 7, Title = "The Lord of the Rings", Author = "J.R.R. Tolkien", Publisher = "Allen & Unwin", YearPublished = 1954, Price = 29.99 },
                new Book { BookId = 8, Title = "One Hundred Years of Solitude", Author = "Gabriel Garcia Marquez", Publisher = "Harper & Row", YearPublished = 1967, Price = 11.50 },
                new Book { BookId = 9, Title = "The Diary of a Young Girl", Author = "Anne Frank", Publisher = "Contact Publishing", YearPublished = 1947, Price = 6.99 },
                new Book { BookId = 10, Title = "The Alchemist", Author = "Paulo Coelho", Publisher = "HarperCollins", YearPublished = 1988, Price = 9.99 },
                new Book { BookId = 11, Title = "The Adventures of Huckleberry Finn", Author = "Mark Twain", Publisher = "Chatto & Windus", YearPublished = 1884, Price = 8.50 }
            );
        }
    }    
}
