﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace BooksAPI.Migrations
{
    /// <inheritdoc />
    public partial class CreateAndSeedBooksTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    BookId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Author = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Publisher = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    YearPublished = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.BookId);
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "BookId", "Author", "Price", "Publisher", "Title", "YearPublished" },
                values: new object[,]
                {
                    { 1, "F. Scott Fitzgerald", 9.9900000000000002, "Charles Scribner's Sons", "The Great Gatsby", 1925 },
                    { 2, "Harper Lee", 12.5, "J. B. Lippincott & Co.", "To Kill a Mockingbird", 1960 },
                    { 3, "J.D. Salinger", 8.9499999999999993, "Little, Brown and Company", "The Catcher in the Rye", 1951 },
                    { 4, "George Orwell", 10.99, "Secker & Warburg", "1984", 1949 },
                    { 5, "J.R.R. Tolkien", 14.99, "Allen & Unwin", "The Hobbit", 1937 },
                    { 6, "Jane Austen", 7.9900000000000002, "T. Egerton, Whitehall", "Pride and Prejudice", 1813 },
                    { 7, "J.R.R. Tolkien", 29.989999999999998, "Allen & Unwin", "The Lord of the Rings", 1954 },
                    { 8, "Gabriel Garcia Marquez", 11.5, "Harper & Row", "One Hundred Years of Solitude", 1967 },
                    { 9, "Anne Frank", 6.9900000000000002, "Contact Publishing", "The Diary of a Young Girl", 1947 },
                    { 10, "Paulo Coelho", 9.9900000000000002, "HarperCollins", "The Alchemist", 1988 },
                    { 11, "Mark Twain", 8.5, "Chatto & Windus", "The Adventures of Huckleberry Finn", 1884 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");
        }
    }
}
