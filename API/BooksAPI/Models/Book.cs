using System.ComponentModel.DataAnnotations;

namespace BooksAPI.Models
{
    public class Book
    {
        [Required]
        [Key]
        public int BookId { get; set; }
        [Required]
        [StringLength(75)]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Publisher { get; set; }
        [Required]
        [Range(1884, 2023)]
        public int YearPublished { get; set; }
        [Required]
        public double Price { get; set; }
    }
}